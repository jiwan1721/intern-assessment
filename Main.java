/*
       DO NOT MODIFY THIS FILE
 */

package com.amtrixtech.internship;

import com.amtrixtech.internship.task.CrazyMath;
import com.amtrixtech.internship.util.CrazyNumbers;

public class Main {

    public static void main(String[] args) {
        CrazyMath crazyMath = new CrazyMath();
        test(crazyMath);
    }

    private static void test(CrazyMath crazyMath) {
        System.out.println(getArray(crazyMath.getNumbersLessThan(10, CrazyNumbers.arr1)));
        System.out.println(getArray(crazyMath.getNumbersLessThan(100, CrazyNumbers.arr2)));
        System.out.println(getArray(crazyMath.getNumbersLessThan(50, CrazyNumbers.arr3)));

        System.out.println(crazyMath.getMostRepeatedNumber(CrazyNumbers.arr4)); // 0
        System.out.println(crazyMath.getMostRepeatedNumber(CrazyNumbers.arr5)); // 100
        System.out.println(crazyMath.getMostRepeatedNumber(CrazyNumbers.arr6)); // 100

        System.out.println(crazyMath.isPrime(10)); // false
        System.out.println(crazyMath.isPrime(17)); // true
        System.out.println(crazyMath.isPrime(177770)); // false
        System.out.println(crazyMath.isPrime(6073)); // true
        System.out.println(crazyMath.isPrime(131071)); // true
    }

    private static String getArray(int[] arr) {
        if (arr == null) return "";
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (int i : arr) {
            sb.append(i + ",");
        }
        sb.append(" ]");
        return sb.toString();
    }
}
