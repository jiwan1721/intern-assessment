/*
       DO NOT MODIFY THIS FILE
 */

package com.amtrixtech.internship.util;

public interface CrazyNumbers {

    static final int[] arr1 = {1,2,3,4,5,10,9,100,50,30,20};
    static final int[] arr2 = {18,19,10,34,70,101,102,200,900,999,99,91};
    static final int[] arr3 = {10,12,11,9,8,199,190,198,56,45,48,197,299,399,499,599,988,856,1234,43,566,87,44,41,39,465};

    static final int[] arr4 = {1,2,3,4,5,1,0,0,0,0,1,10,19,0};
    static final int[] arr5 = {100,19,100,7,8,100,7,10,10,100,10,10,100,100,100,10,80,80,80,80};
    static final int[] arr6 = {8,80,100,8,80,19,19,80,80,80,100,100,100,100};
}
