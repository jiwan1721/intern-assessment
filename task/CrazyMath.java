/*
    YOU CAN WRITE YOUR SOLUTION IN RESPECTIVE METHODS
 */

package com.amtrixtech.internship.task;

public class CrazyMath {
	/*
    You are given an array of integers 'arr'. You have to
    return a copy of array that contains integers which are
    less than but not including 'num'.
    Example: arr = [10,5,7,11,15,20,25,24,9]
             num = 10
          Output: [5,7,9]
 */
public int[] getNumbersLessThan(int num, int[] arr) {
	System.out.println("number less than"+num+" is: ");
	for(int i = 0; i < arr.length; i++) {
		   if(arr[i] < num){
			 
		      System.out.println(arr[i]);
		   }
		}
	return arr;



}

/*
    You are given an array of integers 'arr'. You would have to return the
    integer that is repeated for maximum numbers. If two integers are repeated
    for same amount of number, then you'd have to return the highest integer
    Example 1: arr = [1,4,5,10,4,5,10,1,10,4,4]
              Output = 4 (as 4 is repeated for the most time)
    Example 2: arr = [1,2,3,5,1,5,1,5]
               Output = 5 (here, both 1 and 5 are repeated for three times,
                           but 5 is the highest integer among 1 and 5)
 */
public int getMostRepeatedNumber(int[] arr) {
	 int count = 1, tempCount;
	  int result = arr[0];
	  int temp = 0;
	  for (int i = 0; i < (arr.length - 1); i++)
	  {
	    temp = arr[i];
	    tempCount = 0;
	    for (int j = 1; j < arr.length; j++)
	    {
	      if (temp == arr[j])
	        tempCount++;
	    }
	    if (tempCount > count)
	    {
	      result = temp;
	      count = tempCount;
	    }
	  }
	  return result;
	

}

/*
    Given an integer 'num', you'd have to return true if 'num' is a prime number,
    or else return false if it isn't

    NOTE: TRY TO SOLVE THIS METHOD WITH TIME COMPLEXITY IN MIND
 */
public boolean isPrime(int num) {
	 if (num <= 1) {  
         return false;  
     }  
     for (int i = 2; i < Math.sqrt(num); i++) {  
         if (num % i == 0) {  
             return false;  
         }  
     }  
     return true;  

	}
    
}

}
